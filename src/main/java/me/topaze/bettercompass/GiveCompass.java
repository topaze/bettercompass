package me.topaze.bettercompass;

import de.tr7zw.changeme.nbtapi.NBTItem;
import me.topaze.bettercompass.Items.CustomCompass;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class GiveCompass implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if(!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Vous devez être un joueur pour pouvoir éxécuter cette commande.");
        }

        if(!(player.hasPermission("compass.give"))) {
            sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission necessaire pour éxécuter cette commande.");
        }

        CustomCompass compass = new CustomCompass();

        player.getInventory().addItem(compass.getItemStack());

        return false;
    }
}
