package me.topaze.bettercompass.Items;

import de.tr7zw.changeme.nbtapi.NBTItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class CustomCompass implements Item {
    public NBTItem nbti;

    public CustomCompass() {
        ItemStack compass = new ItemStack(Material.COMPASS);
        ItemMeta compassMeta = (ItemMeta) compass.getItemMeta();

        compassMeta.setDisplayName(String.format("%s%sSuper boussole.", ChatColor.GOLD, ChatColor.BOLD));

        ArrayList<String> lores = new ArrayList<>();
        lores.add("Cliquez sur n'importe quel bloc");
        lores.add("et la boussole pointera vers");
        lores.add("ce point.");

        compassMeta.setLore(lores);
        compass.setItemMeta(compassMeta);

        nbti = new NBTItem(compass);
        nbti.setString("customItemName", "customCompass");

    }

    @Override
    public ItemStack getItemStack() {
        return nbti.getItem();
    }

    @Override
    public String getCustomName(String key) {
        return nbti.getString(key);
    }
}
