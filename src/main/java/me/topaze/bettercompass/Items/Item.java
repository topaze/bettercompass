package me.topaze.bettercompass.Items;

import org.bukkit.inventory.ItemStack;

public interface Item {
    public ItemStack getItemStack();

    public String getCustomName(String key);
}
