package me.topaze.bettercompass;

import me.topaze.bettercompass.Items.CustomCompass;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.java.JavaPlugin;

public class BetterCompass extends JavaPlugin {
    @Override
    public void onEnable() {
        Bukkit.getLogger().info("BetterCompass activé.");

        // Customs recipies
        CustomCompass compass = new CustomCompass();

        NamespacedKey key = new NamespacedKey(this, "compass");
        ShapedRecipe recipe = new ShapedRecipe(key, compass.getItemStack());

        recipe.shape("ODO", "FBF", "OFO");

        recipe.setIngredient('O', Material.GOLD_INGOT);
        recipe.setIngredient('F', Material.IRON_INGOT);
        recipe.setIngredient('D', Material.DIAMOND);
        recipe.setIngredient('B', Material.COMPASS);

        Bukkit.addRecipe(recipe);


        // Commands
        this.getCommand("compass").setExecutor(new GiveCompass());

        // Listener
        this.getServer().getPluginManager().registerEvents(new PlayerInteractionListener(), this);
    }
}
