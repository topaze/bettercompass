package me.topaze.bettercompass;

import de.tr7zw.changeme.nbtapi.NBTItem;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class PlayerInteractionListener implements Listener {
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        ItemStack playerItem = e.getPlayer().getInventory().getItemInMainHand();
        ItemMeta playerItemMeta = (ItemMeta) playerItem.getItemMeta();


        NBTItem nbti = new NBTItem(playerItem);

        if(!(player.hasPermission("compass.relocate"))) {
            player.sendMessage(ChatColor.RED + "Vous n'avez pas la permission de pointer un nouveau bloc.");
        }

        else if(nbti.getString("customItemName").equals("customCompass") && e.getAction().name().equals("LEFT_CLICK_BLOCK")) {

            try {
                Location clickedBlocLoc = e.getClickedBlock().getLocation();
                ArrayList<String> lores = new ArrayList<>();
                lores.add("Cette boussole point vers");
                lores.add(String.format("x: %d", Math.round(clickedBlocLoc.getX())));
                lores.add(String.format("y: %d", Math.round(clickedBlocLoc.getY())));
                lores.add(String.format("z: %d", Math.round(clickedBlocLoc.getZ())));
                playerItemMeta.setLore(lores);
                playerItem.setItemMeta(playerItemMeta);

                clickedBlocLoc.getWorld().spawnParticle(Particle.END_ROD, clickedBlocLoc, 20);
                clickedBlocLoc.getWorld().playSound(clickedBlocLoc, Sound.BLOCK_NOTE_BLOCK_BELL, 1, 1);
                player.sendMessage(String.format("%s%sLa boussole pointe vers un nouveau bloc.", ChatColor.GRAY, ChatColor.ITALIC));

                player.setCompassTarget(clickedBlocLoc);
            } catch (NullPointerException exception) {

            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player player = e.getPlayer();
        ItemStack playerItem = e.getPlayer().getInventory().getItemInMainHand();
        NBTItem nbti = new NBTItem(playerItem);

        if(nbti.getString("customItemName").equals("customCompass")) {

            try {

                e.setCancelled(true);

            } catch (NullPointerException exception) {

            }
        }
    }
}
